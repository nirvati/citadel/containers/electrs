ARG VERSION=v0.10.7

FROM rust:1.83.0-bookworm AS electrs-build

ARG VERSION

RUN apt-get update
RUN apt-get install -qq -y clang cmake git
#RUN rustup component add rustfmt

# Build, test and install electrs
WORKDIR /build/electrs
RUN git clone --depth=1 --branch $VERSION https://github.com/romanz/electrs .
RUN cargo build --locked --release --all

FROM debian:bookworm-slim

COPY --from=electrs-build /build/electrs/target/release/electrs /usr/bin/electrs

RUN groupadd -r user && adduser --disabled-login --system --shell /bin/false --uid 1000 --ingroup user user

USER user

# Electrum RPC
EXPOSE 50001

# Prometheus monitoring
EXPOSE 4224

STOPSIGNAL SIGINT

ENTRYPOINT ["/usr/bin/electrs"]